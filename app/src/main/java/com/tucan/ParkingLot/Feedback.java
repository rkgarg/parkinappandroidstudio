package com.tucan.ParkingLot;

import com.parse.GetCallback;
import com.parse.ParseException;
import com.parse.ParseObject;
import com.parse.ParseQuery;
import com.parse.SaveCallback;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.view.View.OnClickListener;
import android.widget.ImageButton;

public class Feedback extends Activity {
	ImageButton tick, cancel, happy, sad;
	Boolean isSatisfy = false, isHappy = false, IsDone = false;
	String deviceId, car, email;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
				WindowManager.LayoutParams.FLAG_FULLSCREEN);

		super.onCreate(savedInstanceState);
		setContentView(R.layout.feedback);

		deviceId = "user"
				+ ParkingCommon.getinstance(Feedback.this).getDeviceID();
		car = ParkingCommon.getinstance(Feedback.this).getCarId();
		email = ParkingCommon.getinstance(Feedback.this).getEmail();

		tick = (ImageButton) findViewById(R.id.tick);
		tick.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				CheckInternetConnection cd = new CheckInternetConnection(getApplicationContext());
				Boolean IsInternetPresent = cd.isConnection();
				isSatisfy = true;
				tick.setBackgroundResource(R.drawable.statechangetime);
				cancel.setBackgroundResource(R.drawable.custom_border);
				IsDone = true;
				if (IsInternetPresent == false) {
					ParkingCommon.getinstance(Feedback.this)
					.IsInternetPresent();
				}else{
				review();
				}
			}
		});
		cancel = (ImageButton) findViewById(R.id.cancel);
		cancel.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View arg0) {
				// TODO Auto-generated method stub
				/*isSatisfy = false;
				cancel.setBackgroundResource(R.drawable.statechangetime);
				tick.setBackgroundResource(R.drawable.custom_border);
				IsDone = true;*/
				Intent intent=new Intent(Feedback.this,NegativeFeedbackActivity.class);
				startActivity(intent);
				finish();
			}
		});

		happy = (ImageButton) findViewById(R.id.happy);
		sad = (ImageButton) findViewById(R.id.sad);

		/*happy.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				if (IsDone == true) {
					isHappy = true;
					happy.setBackgroundResource(R.drawable.statechangetime);
					sad.setBackgroundResource(R.drawable.custom_border);
					review();
				}
			}
		});
	
			sad.setOnClickListener(new OnClickListener() {

				@Override
				public void onClick(View v) {
					// TODO Auto-generated method stub
					if (IsDone == true) {
						isHappy = false;
						sad.setBackgroundResource(R.drawable.statechangetime);
						happy.setBackgroundResource(R.drawable.custom_border);
						review();
					}
				}
			});*/
	}

	public void review() {
		final ProgressDialog progressDialog = new ProgressDialog(Feedback.this);
		progressDialog.setMessage("Por favor,espera.....");
		progressDialog.setIndeterminate(true);
		progressDialog.setCancelable(false);
		progressDialog.show();

		ParseObject pasobj = new ParseObject(Prefs.table_feedback);
		pasobj.put(Prefs.table_col_isStatisfied, isSatisfy);
		pasobj.put("FeedbackText", "");
		//pasobj.put(Prefs.table_col_ishappy, isHappy);
		pasobj.put(Prefs.table_col_carid, car);
		pasobj.put(Prefs.table_col_email, email);
		pasobj.saveInBackground(new SaveCallback() {

			@Override
			public void done(ParseException e) {
				progressDialog.dismiss();
				Intent in = new Intent(Feedback.this, ThanksForRating.class);
				startActivity(in);
				Feedback.this.finish();
			}
		});

	}

}
