package com.tucan.ParkingLot;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.provider.Settings.Secure;
import android.util.Log;

public class ParkingCommon {
	public static ParkingCommon _instance = null;
	public static Context context;
	RegisteredCars refrernce;

	public RegisteredCars getRefrernce() {
		return refrernce;
	}

	public void setRefrernce(RegisteredCars refrernce) {
		this.refrernce = refrernce;
	}

	public static ParkingCommon getinstance(Context _context) {
		if (_instance == null)
			_instance = new ParkingCommon();
		context = _context;
		return _instance;

	}
	public void setNotificationCount(int notificationCount) {
		SharedPreferences sharedPreferences = context.getSharedPreferences(
				Prefs.local_cust_table, Context.MODE_PRIVATE);
		SharedPreferences.Editor editor = sharedPreferences.edit();
		editor.putInt(Prefs.notificationCount, notificationCount);
		editor.commit();
	}

	public int getNotificationCount() {
		SharedPreferences sharedPreferences = context.getSharedPreferences(
				Prefs.local_cust_table, Context.MODE_PRIVATE);
		int notificationCount = sharedPreferences.getInt(Prefs.notificationCount, 0);
		return notificationCount;

	}
	// Set Data of Admin
	public void setDataofAdmin(String name, String password) {
		SharedPreferences sharedPreferences = context.getSharedPreferences(
				Prefs.local_cust_table, context.MODE_PRIVATE);
		SharedPreferences.Editor editor = sharedPreferences.edit();
		editor.putString(Prefs.Adminname, name);
		editor.putString(Prefs.Adminpass, password);
		editor.commit();
	}

	public String getDeviceIDAdmin() {
		String android_id = Secure.getString(context.getContentResolver(),
				Secure.ANDROID_ID);
		return android_id;
	}

	public void setCarId(String carid) {
		SharedPreferences sharedPreferences = context.getSharedPreferences(
				Prefs.local_cust_table, Context.MODE_PRIVATE);
		SharedPreferences.Editor editor = sharedPreferences.edit();

		editor.putString(Prefs.local_col_carid, carid);
		editor.commit();
	}

	public String getCarId() {
		SharedPreferences sharedPreferences = context.getSharedPreferences(
				Prefs.local_cust_table, Context.MODE_PRIVATE);
		String car = sharedPreferences.getString(Prefs.local_col_carid, "");
		return car;

	}
	public void setAdminLogin(Boolean carid) {
		SharedPreferences sharedPreferences = context.getSharedPreferences(
				Prefs.local_cust_table, Context.MODE_PRIVATE);
		SharedPreferences.Editor editor = sharedPreferences.edit();

		editor.putBoolean(Prefs.isAdminLogin, carid);
		editor.commit();
	}

	public Boolean getAdminLogin() {
		SharedPreferences sharedPreferences = context.getSharedPreferences(
				Prefs.local_cust_table, Context.MODE_PRIVATE);
		Boolean car = sharedPreferences.getBoolean(Prefs.isAdminLogin, false);
		return car;

	}
	public void setTermsSelected(Boolean carid) {
		SharedPreferences sharedPreferences = context.getSharedPreferences(
				Prefs.local_cust_table, Context.MODE_PRIVATE);
		SharedPreferences.Editor editor = sharedPreferences.edit();
		editor.putBoolean(Prefs.isTermsAccepted, carid);
		editor.commit();
	}

	public Boolean getTermsSelected() {
		SharedPreferences sharedPreferences = context.getSharedPreferences(
				Prefs.local_cust_table, Context.MODE_PRIVATE);
		Boolean car = sharedPreferences.getBoolean(Prefs.isTermsAccepted, false);
		return car;

	}
	public void setEmail(String email) {
		SharedPreferences sharedPreferences = context.getSharedPreferences(
				Prefs.local_cust_table, Context.MODE_PRIVATE);
		SharedPreferences.Editor editor = sharedPreferences.edit();

		editor.putString(Prefs.local_col_email, email);
		editor.commit();
	}

	public String getEmail() {
		SharedPreferences sharedPreferences = context.getSharedPreferences(
				Prefs.local_cust_table, Context.MODE_PRIVATE);
		String email = sharedPreferences.getString(Prefs.local_col_email, "");
		return email;

	}

	public void setAllInfo(String name, String id, String carid, String email,
			String phone, String device) {
		SharedPreferences sharedPreferences = context.getSharedPreferences(
				Prefs.local_cust_table, Context.MODE_PRIVATE);
		SharedPreferences.Editor editor = sharedPreferences.edit();

		editor.putString(Prefs.local_col_name, name);
		editor.putString(Prefs.local_col_id, id);
		editor.putString(Prefs.local_col_carid, carid);
		editor.putString(Prefs.loacl_col_email, email);
		editor.putString(Prefs.local_col_phone, phone);
		editor.putString(Prefs.local_col_deviceid, device);
		editor.commit();
	}

	public String getDeviceID() {
		String android_id = Secure.getString(context.getContentResolver(),
				Secure.ANDROID_ID);
		return android_id;
	}

	public void okAlertDialog(String message) {
		AlertDialog.Builder builder = new AlertDialog.Builder(context);
		builder.setMessage(message).setCancelable(false)
				.setPositiveButton("OK", new DialogInterface.OnClickListener() {
					public void onClick(DialogInterface dialog, int id) {

					}
				});
		AlertDialog alert = builder.create();
		alert.show();
	}

	public void isAdmin(Boolean IsAdmin) {
		SharedPreferences sharedPreferences = context.getSharedPreferences(
				Prefs.local_cust_table, context.MODE_PRIVATE);
		SharedPreferences.Editor editor = sharedPreferences.edit();
		editor.putBoolean("IsAdmin", IsAdmin);
		editor.commit();
	}

	public void IsInternetPresent() {
		AlertDialog.Builder builder = new AlertDialog.Builder(context);
		builder.setTitle("OOPS");
		builder.setMessage("Sin Conexión A Internet").setCancelable(false)
				.setPositiveButton("OK", new DialogInterface.OnClickListener() {
					public void onClick(DialogInterface dialog, int id) {
						// do things
					}
				});
		AlertDialog alert = builder.create();
		alert.show();
	}

	public void InValidAdmin() {
		AlertDialog.Builder builder = new AlertDialog.Builder(context);
		builder.setTitle("OOPS");
		builder.setMessage(
				"Información no valida, por favor verifica nuevamente")
				.setCancelable(false)
				.setPositiveButton("OK", new DialogInterface.OnClickListener() {
					public void onClick(DialogInterface dialog, int id) {
						// do things
					}
				});
		AlertDialog alert = builder.create();
		alert.show();
	}

	public void CarIDRegisted() {
		AlertDialog.Builder builder = new AlertDialog.Builder(context);

		builder.setMessage("El vehículo ha sido registrado exitosamente")
				.setCancelable(false)
				.setPositiveButton("OK", new DialogInterface.OnClickListener() {
					public void onClick(DialogInterface dialog, int id) {
						((Activity)context).finish();
					}
				});
		AlertDialog alert = builder.create();
		alert.show();
	}

	public void NoRegistedCar() {
		AlertDialog.Builder builder = new AlertDialog.Builder(context);

		builder.setMessage("No hay vehículos registrados").setCancelable(false)
				.setPositiveButton("OK", new DialogInterface.OnClickListener() {
					public void onClick(DialogInterface dialog, int id) {
						// do things
					}
				});
		AlertDialog alert = builder.create();
		alert.show();
	}
	public String returnCarId(String carid){
		Log.v("carid", carid);
		String newCarId=	carid.replaceAll(" ", "");
		Log.v("new carid", newCarId);
			return newCarId;
	}
}
