package com.tucan.ParkingLot;

import java.util.List;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.parse.FindCallback;
import com.parse.GetCallback;
import com.parse.ParseException;
import com.parse.ParseObject;
import com.parse.ParseQuery;

public class CarRegistration extends Activity implements OnClickListener {
	String CarID;
	Boolean IsInternetPresent = false;
	CheckInternetConnection cd;
	EditText Carid;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
				WindowManager.LayoutParams.FLAG_FULLSCREEN);
		super.onCreate(savedInstanceState);
		setContentView(R.layout.car_registration);
		cd = new CheckInternetConnection(getApplicationContext());
		Carid = (EditText) findViewById(R.id.PlacaEditText);

		MyTextView saveCarId = (MyTextView) findViewById(R.id.Guardar);
		saveCarId.setOnClickListener(this);

		Button backBtn = (Button) findViewById(R.id.backButton);
		backBtn.setOnClickListener(this);

	}

	@Override
	public void onClick(View v) {
		if (v.getId() == R.id.backButton) {

			CarRegistration.this.finish();
		} else if (v.getId() == R.id.Guardar) {
			IsInternetPresent = cd.isConnection();
			if (IsInternetPresent == true) {

				CarID = ParkingCommon.getinstance(CarRegistration.this).returnCarId(Carid.getText().toString().trim().toUpperCase());
				if (errorMessage()) {
					// CarIdDoesNotExist();
					carRegister();
				}
			} else {
				ParkingCommon.getinstance(CarRegistration.this)
						.IsInternetPresent();
			}
		}

	}

	public Boolean errorMessage() {
		if (Carid.getText().toString().trim().length() == 0) {
			Carid.setError(getResources().getString(R.string.el_carid));
			return false;
		}
		return true;
	}

	public void CarIdDoesNotExist() {
		final ProgressDialog progressDialog = new ProgressDialog(
				CarRegistration.this);
		progressDialog.setMessage("Por Favor Espere ...");
		progressDialog.setIndeterminate(true);
		progressDialog.setCancelable(false);
		progressDialog.show();

		ParseQuery<ParseObject> query = ParseQuery.getQuery("UserData");

		query.whereEqualTo("CarID", CarID);

		query.findInBackground(new FindCallback<ParseObject>() {

			@Override
			public void done(List<ParseObject> objects, ParseException e) {

				if (objects.isEmpty()) {

					progressDialog.dismiss();
					alertForCarIDDoesNotExist();
				} else {
					carRegister();
					progressDialog.dismiss();
				}
			}

		});
	}

	public void carRegister() {
		if (errorMessage()) {

			final ProgressDialog progressDialog = new ProgressDialog(
					CarRegistration.this);
			progressDialog.setMessage("Por Favor Espere ...");
			progressDialog.setIndeterminate(true);
			progressDialog.setCancelable(false);
			progressDialog.show();
			ParseQuery<ParseObject> query = ParseQuery
					.getQuery(Prefs.tableRegistedCars);
			query.whereEqualTo(Prefs.tableRegistedCars_column, CarID);
			query.getFirstInBackground(new GetCallback<ParseObject>() {

				@Override
				public void done(ParseObject object, ParseException e) {
					if (object == null) {
						Log.v("carID", CarID);
						ParseObject pasobj = new ParseObject("RegistedCar");
						pasobj.put(Prefs.tableRegistedCars_column, CarID);
						pasobj.put(Prefs.tableRegistedCars_IsReady, false);
						pasobj.put(Prefs.tableRegistedCars_IsDelivered, false);
						pasobj.put("TimeSelected", "");
						pasobj.saveInBackground();
								CarIDRegisted();
						
					} else {
						alertForAlreadyRegisted();
					}
					progressDialog.dismiss();
				}
			});
		}
	}
	public void CarIDRegisted() {
		AlertDialog.Builder builder = new AlertDialog.Builder(CarRegistration.this);

		builder.setMessage("El vehículo ha sido registrado exitosamente")
				.setCancelable(false)
				.setPositiveButton("OK", new DialogInterface.OnClickListener() {
					public void onClick(DialogInterface dialog, int id) {
						finish();
					}
				});
		AlertDialog alert = builder.create();
		alert.show();
	}

	public void alertForAlreadyRegisted() {
		AlertDialog.Builder builder = new AlertDialog.Builder(this);
		builder.setTitle("IMPORTANTE");
		builder.setMessage(getResources().getString(R.string.esta_register))
				.setCancelable(false)
				.setPositiveButton("OK", new DialogInterface.OnClickListener() {
					public void onClick(DialogInterface dialog, int id) {
						// do things
					}
				});
		AlertDialog alert = builder.create();
		alert.show();
	}

	public void alertForCarIDDoesNotExist() {
		AlertDialog.Builder builder = new AlertDialog.Builder(this);
		builder.setMessage(getResources().getString(R.string.carid_exist))
				.setCancelable(false)
				.setPositiveButton("OK", new DialogInterface.OnClickListener() {
					public void onClick(DialogInterface dialog, int id) {

					}
				});
		AlertDialog alert = builder.create();
		alert.show();
	}
}