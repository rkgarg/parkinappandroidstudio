package com.tucan.ParkingLot;

import android.app.ActivityManager;
import android.app.AlarmManager;
import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.media.RingtoneManager;
import android.net.Uri;
import android.support.v4.app.NotificationCompat;
import android.util.Log;

import java.util.Calendar;
import java.util.Date;
import java.util.List;

import com.parse.FindCallback;
import com.parse.GetCallback;
import com.parse.ParseException;
import com.parse.ParseInstallation;
import com.parse.ParseObject;
import com.parse.ParsePush;
import com.parse.ParseQuery;

public class AlarmReceiver extends BroadcastReceiver {
	private NotificationManager mNotificationManager;
	private int SIMPLE_NOTFICATION_ID = 1;
	Notification myNotification;
	CharSequence notificationTitle;
	CharSequence notificationText;
	String[] notification_array;

	@Override
	public void onReceive(Context context, Intent intent) {
		String car = intent.getStringExtra("car_id");
		Log.v("receiver", "alarm receiver");
		sendNotification(context, car);

	}

	private void sendNotification(final Context context, final String car) {
		ParseQuery<ParseObject> putTime = ParseQuery
				.getQuery(Prefs.table_registedcar);
		putTime.whereEqualTo(Prefs.table_col_carid, car);
		putTime.getFirstInBackground(new GetCallback<ParseObject>() {

			@Override
			public void done(ParseObject object, ParseException e) {
				if (object != null) {

					ParseQuery<ParseObject> putTime1 = ParseQuery
							.getQuery(Prefs.table_adminDeviceID);
					putTime1.whereExists(Prefs.table_col_deviceid);
					putTime1.setLimit(1000);
					putTime1.findInBackground(new FindCallback<ParseObject>() {

						@Override
						public void done(List<ParseObject> list,
								ParseException e) {
							if (e == null) {
								if (list.size() > 0) {
									for (int i = 0; i < list.size(); i++) {
										ParseObject obj = list.get(i);
										String android_id = obj
												.getString(Prefs.table_col_deviceid);
										ParseQuery query1 = ParseInstallation
												.getQuery();
										query1.whereEqualTo("channels",
												android_id);
										ParsePush push = new ParsePush();
										push.setQuery(query1);
										String message =" BAJAR "+ car;
										push.setMessage(message);// Hey i need
																	// my car,
																	// my
										// Car Id is "+ car
										push.sendInBackground();

									}

								}
							}

						}
					});
				}
			}
		});

	}

}
