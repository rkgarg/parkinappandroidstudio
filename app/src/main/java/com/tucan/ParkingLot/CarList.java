package com.tucan.ParkingLot;

public class CarList {
	String Carid = " ";
	Boolean IsReadyButton = false;
	Boolean IsDeliveredButton = false;

	public CarList(String Carid, Boolean IsReadyButton, Boolean IsDeliveredButton) {
		super();
		this.Carid = Carid;
		this.IsReadyButton = IsReadyButton;
		
		this.IsDeliveredButton = IsDeliveredButton;

	}

	public String getCarid() {
		return Carid;
	}

	public void setCarid(String carid) {
		Carid = carid;
	}

	public Boolean getCheckBox1() {
		return IsReadyButton;
	}

	public void setCheckBox1(Boolean IsReadyButton) {
		this.IsReadyButton = IsReadyButton;
	}

	public Boolean getCheckBox2() {
		return IsDeliveredButton;
	}

	public void setCheckBox2(Boolean IsDeliveredButton) {
		this.IsDeliveredButton = IsDeliveredButton;
	}

}
