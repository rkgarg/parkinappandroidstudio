package com.tucan.ParkingLot;

import com.parse.ParseException;
import com.parse.ParseObject;
import com.parse.SaveCallback;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

public class NegativeFeedbackActivity extends Activity {
EditText feedback;
TextView send_feedback;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_negative_feedback);
		feedback=(EditText)findViewById(R.id.feedback_text);
		send_feedback=(TextView)findViewById(R.id.send);
		send_feedback.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				CheckInternetConnection cd = new CheckInternetConnection(getApplicationContext());
				Boolean IsInternetPresent = cd.isConnection();
				if(feedback.getText().toString().compareTo("")==0){
				Toast.makeText(NegativeFeedbackActivity.this, "Debes escribir un comentario para continuar", Toast.LENGTH_SHORT).show();
				}else if (IsInternetPresent == false) {
					ParkingCommon.getinstance(NegativeFeedbackActivity.this)
					.IsInternetPresent();
				}else{
					review();
				}
			}
		});
	}
	public void review() {
		final ProgressDialog progressDialog = new ProgressDialog(NegativeFeedbackActivity.this);
		progressDialog.setMessage("Por favor,espera.....");
		progressDialog.setIndeterminate(true);
		progressDialog.setCancelable(false);
		progressDialog.show();

		ParseObject pasobj = new ParseObject(Prefs.table_feedback);
		pasobj.put(Prefs.table_col_isStatisfied, false);
		pasobj.put("FeedbackText", feedback.getText().toString());
		//pasobj.put(Prefs.table_col_ishappy, isHappy);
		pasobj.put(Prefs.table_col_carid,  ParkingCommon.getinstance(NegativeFeedbackActivity.this).getCarId());
		pasobj.put(Prefs.table_col_email, ParkingCommon.getinstance(NegativeFeedbackActivity.this).getEmail());
		pasobj.saveInBackground(new SaveCallback() {

			@Override
			public void done(ParseException e) {
				progressDialog.dismiss();
				if(e==null){
					sendFeedbackDialog("Tu comentario ha sido entregado exitosamente!");
				}
				else{
					e.printStackTrace();
				}
			}
		});

	}

    public void sendFeedbackDialog(String messageName) {
        AlertDialog.Builder alertDialog = new AlertDialog.Builder(NegativeFeedbackActivity.this);
        alertDialog.setTitle("");
        alertDialog.setMessage(messageName);
        alertDialog.setNeutralButton("Ok", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
                finish();
				System.exit(0);
				NegativeFeedbackActivity.this.finish();
            }
        });
        alertDialog.show();
    }

}
