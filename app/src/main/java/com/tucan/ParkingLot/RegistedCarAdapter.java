package com.tucan.ParkingLot;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageButton;
import android.widget.ListView;
import android.widget.TextView;

import com.parse.FindCallback;
import com.parse.GetCallback;
import com.parse.ParseException;
import com.parse.ParseInstallation;
import com.parse.ParseObject;
import com.parse.ParsePush;
import com.parse.ParseQuery;

public class RegistedCarAdapter extends BaseAdapter {
	ArrayList<CarList> CarIDArrayList;
	ListView listview;
	Context context;

	CarList carList;

	public RegistedCarAdapter(Context _context,
			ArrayList<CarList> carArrayList, ListView listview1) {
		context = _context;

		this.CarIDArrayList = carArrayList;
		this.listview = listview1;
		// TODO Auto-generated constructor stub
	}

	@Override
	public int getCount() {

		return CarIDArrayList.size();
	}

	@Override
	public Object getItem(int position) {
		return CarIDArrayList.get(position);
	}

	@Override
	public long getItemId(int position1) {

		return position1;
	}

	@Override
	public View getView(int position, View row, ViewGroup group) {
		if (row == null) {
			LayoutInflater inflater = (LayoutInflater) RegistedCarAdapter.this.context
					.getSystemService(context.LAYOUT_INFLATER_SERVICE);
			row = inflater.inflate(R.layout.row_registered_cars, group, false);
		}

		TextView textView = (TextView) row.findViewById(R.id.CarID);
		ImageButton isReadyButton = (ImageButton) row
				.findViewById(R.id.IsReadyButton);
		final ImageButton isDeliveredButton = (ImageButton) row
				.findViewById(R.id.IsDeliveredButton);
		carList = CarIDArrayList.get(position);
		isReadyButton.setTag(Integer.toString(position));
		isDeliveredButton.setTag(Integer.toString(position));
		isReadyButton.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(final View view) {
				final ProgressDialog progressDialog = new ProgressDialog(
						context);
				progressDialog.setMessage("Cargando...");
				progressDialog.setIndeterminate(true);
				progressDialog.setCancelable(false);
				progressDialog.show();

				int userTime = 0;

				Calendar c = Calendar.getInstance();

				SimpleDateFormat sdf = new SimpleDateFormat(
						"yyyy-MM-dd 'at' HH:mm:ss");
				c.add(Calendar.MINUTE, userTime);
				final String strDate = sdf.format(c.getTime());
				final int positioncheck = Integer.parseInt(view.getTag()
						.toString());
				int position2 = Integer.parseInt(view.getTag().toString());
				final CarList car = CarIDArrayList.get(position2);
				String carIDForCheck = car.getCarid();
				ParseQuery<ParseObject> query = new ParseQuery("RegistedCar");
				query.whereEqualTo("CarID", carIDForCheck);

				query.getFirstInBackground(new GetCallback<ParseObject>() {

					@Override
					public void done(ParseObject object, ParseException e) {
						if (object != null) {
							if (car.getCheckBox1()) {
								object.put("IsReady", false);
								object.saveInBackground();
								car.setCheckBox1(false);
								String carid = car.getCarid();
								String message = "El vehículo " + carid
										+ " esta listo para ser entregada";
								sendNotificationOnReady(carid, message);
								view.setBackgroundResource(R.drawable.customborders);
								progressDialog.dismiss();
							} else {
								CarList carReady = CarIDArrayList
										.get(positioncheck);
								object.put("IsReady", true);
								object.saveInBackground();
								String carid = car.getCarid();
								String message = "El vehículo " + carid
										+ " esta listo para ser entregada";
								sendNotificationOnReady(carid, message);
								sendNotificationToUserWhenCarReady(carid);
								car.setCheckBox1(true);
								view.setBackgroundResource(R.drawable.statetickchange);
								progressDialog.dismiss();

							}

						}
					}
				});

			}
		});
		isDeliveredButton.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View view) {

				int position2 = Integer.parseInt(view.getTag().toString());
				CarList car = CarIDArrayList.get(position2);
				if (car.getCheckBox1() == true) {

					if (car.getCheckBox2()) {
						car.setCheckBox2(false);
						((ImageButton) view)
								.setImageResource(R.drawable.key_select);

					} else {

						String carid = car.getCarid();
						alertDialogue(carid, position2);
						((ImageButton) view).setImageResource(R.drawable.key);
					}
				}

			}
		});
		textView.setText(carList.Carid);

		if (carList.getCheckBox1()) {

			isReadyButton.setBackgroundResource(R.drawable.statetickchange);
		} else {
			isReadyButton.setBackgroundResource(R.drawable.customborders);
		}

		if (carList.getCheckBox2()) {
			isDeliveredButton.setImageResource(R.drawable.key_select);
		} else {
			isDeliveredButton.setImageResource(R.drawable.key);
		}
		return row;

	}

	public void sendNotificationToUserWhenCarReady(String carID) {

		ParseQuery<ParseObject> querynotify = ParseQuery.getQuery("UserData");
		querynotify.whereEqualTo("CarID", carID);
		querynotify.findInBackground(new FindCallback<ParseObject>() {

			@Override
			public void done(List<ParseObject> list, ParseException e) {
				if (e == null) {
					if (list.size() > 0) {
						for (int i = 0; i < list.size(); i++) {
							ParseObject obj = list.get(i);
							String UserDeviceID = obj.getString("DeviceID");
							ParsePush push = new ParsePush();
							push.setChannel(UserDeviceID);
							String message = context.getResources().getString(
									R.string.isReadyNotificationText);
							push.setMessage(message);
							push.sendInBackground();
						}
					}
				} else {
					e.printStackTrace();
				}
			}
		});
	}

	public void alertDialogue(final String carid, final int position) {
		AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(
				(RegisteredCars) context);

		// set title
		alertDialogBuilder.setTitle("NOTA");

		// set dialog message
		alertDialogBuilder
				.setMessage(
						context.getResources().getString(R.string.confirm_que))
				.setCancelable(false)
				.setPositiveButton("OK", new DialogInterface.OnClickListener() {
					public void onClick(DialogInterface dialog, int id) {

						ParseQuery<ParseObject> query = ParseQuery
								.getQuery(Prefs.tableRegistedCars);
						query.whereEqualTo("CarID", carid);
						query.findInBackground(new FindCallback<ParseObject>() {
							public void done(List<ParseObject> invites,
									ParseException e) {
								if (e == null) {
									// iterate over all messages and
									// delete them
									if (invites.size() != 0) {
										for (ParseObject invite : invites) {
											invite.deleteInBackground();
											CarList item = CarIDArrayList
													.get(position);
											CarIDArrayList.remove(position);
											ParseQuery<ParseObject> querynotify = ParseQuery
													.getQuery("UserData");
											querynotify.whereEqualTo("CarID",
													carid);

											querynotify
													.findInBackground(new FindCallback<ParseObject>() {

														@Override
														public void done(
																List<ParseObject> list,
																ParseException e) {
															if (e == null) {
																if (list.size() > 0) {
																	for (int i = 0; i < list
																			.size(); i++) {
																		ParseObject object = list
																				.get(i);
																		Log.v("parse object",
																				" "
																						+ object);
																		// try{
																		String UserDeviceID = object
																				.getString("DeviceID");

																		ParsePush push = new ParsePush();
																		push.setChannel(UserDeviceID);
																		String message = context
																				.getResources()
																				.getString(
																						R.string.car_delivered_text)
																				+ " "
																				+ carid;
																		push.setMessage(message);
																		push.sendInBackground();
																		sendNotification(carid);
																		// }catch(ArrayIndexOutOfBoundsException
																		// e1){
																		// e1.printStackTrace();
																		// }
																	}
																}
															} else {
																e.printStackTrace();
															}
														}
													});

											RegistedCarAdapter.this
													.notifyDataSetChanged();

										}
									}
								} else {

								}
							}
						});
					}
				})
				.setNegativeButton("Cancelar",
						new DialogInterface.OnClickListener() {
							public void onClick(DialogInterface dialog, int id) {

								// listview.getPositionForView(listview).
								View v = listview.getChildAt(position);
								ImageButton btn = (ImageButton) v
										.findViewById(R.id.IsDeliveredButton);

								btn.setBackgroundResource(R.drawable.customborders);
								dialog.cancel();
							}
						});

		// create alert dialog
		AlertDialog alertDialog = alertDialogBuilder.create();
		// show it
		alertDialog.show();
	}

	private void sendNotification(final String carID) {
		final ArrayList<String> device_ID = new ArrayList<String>();
		ParseQuery<ParseObject> sendnotification = ParseQuery
				.getQuery("AdminDeviceID");
		sendnotification.whereExists("DeviceID");
		sendnotification.setLimit(1000);
		sendnotification.findInBackground(new FindCallback<ParseObject>() {

			@Override
			public void done(List<ParseObject> list, ParseException e) {
				if (e == null) {
					if (list.size() > 0) {
						String deviceid = "user"
								+ ParkingCommon.getinstance(context)
										.getDeviceID();
						for (int i = 0; i < list.size(); i++) {
							ParseObject obj = list.get(i);
							String android_id = obj.getString("DeviceID");
							if (!deviceid.equals(android_id) && device_ID.contains(android_id)) {
								device_ID.add(android_id);
								ParseQuery query1 = ParseInstallation
										.getQuery();
								query1.whereEqualTo("channels", android_id);
								ParsePush push = new ParsePush();
								push.setQuery(query1);
								push.setMessage("El vehículo " + carID
										+ " ha sido entregado exitosamente");
								push.sendInBackground();
							}

						}

					}
				}

			}
		});
	}

	private void sendNotificationOnReady(final String carID, final String msg) {
		final ArrayList<String> device_ID = new ArrayList<String>();
		ParseQuery<ParseObject> sendnotification = ParseQuery
				.getQuery("AdminDeviceID");
		sendnotification.whereExists("DeviceID");
		sendnotification.setLimit(1000);
		sendnotification.findInBackground(new FindCallback<ParseObject>() {

			@Override
			public void done(List<ParseObject> list, ParseException e) {
				if (e == null) {
					if (list.size() > 0) {
						ParkingCommon getinstance = ParkingCommon
								.getinstance(context);
						String deviceid = "user" + getinstance.getDeviceID();
						for (int i = 0; i < list.size(); i++) {
							ParseObject obj = list.get(i);
							String android_id = obj.getString("DeviceID");
							if (!deviceid.equals(android_id)
									&& !device_ID.contains(android_id)) {
								device_ID.add(android_id);
								ParseQuery query1 = ParseInstallation
										.getQuery();
								query1.whereEqualTo("channels", android_id);
								ParsePush push = new ParsePush();
								push.setQuery(query1);
								push.setMessage(msg);
								push.sendInBackground();
							}
						}

					}
				}

			}
		});
	}
	
	
	
}
