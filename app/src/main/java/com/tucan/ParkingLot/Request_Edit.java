package com.tucan.ParkingLot;

import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;

import com.parse.GetCallback;
import com.parse.ParseException;
import com.parse.ParseObject;
import com.parse.ParseQuery;

import android.app.Activity;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.Toast;

public class Request_Edit extends Activity implements OnClickListener {

	ImageButton popupbtn, editbtn;
	String deviceId;
	ImageButton Oviedo;
	CheckInternetConnection cd;
	Boolean isInternetPresent = false;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
				WindowManager.LayoutParams.FLAG_FULLSCREEN);
		super.onCreate(savedInstanceState);
		setContentView(R.layout.request_edit);
		cd = new CheckInternetConnection(getApplicationContext());
		AdView mAdView = (AdView) findViewById(R.id.adView);
		AdRequest adRequest = new AdRequest.Builder().build();
		mAdView.loadAd(adRequest);

		deviceId = "user"
				+ ParkingCommon.getinstance(Request_Edit.this).getDeviceID();

		popupbtn = (ImageButton) findViewById(R.id.timeselect);
		popupbtn.setOnClickListener(this);
		editbtn = (ImageButton) findViewById(R.id.edit);
		editbtn.setOnClickListener(this);
		Oviedo = (ImageButton) findViewById(R.id.button1);
		Oviedo.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View arg0) {
				Intent intent = new Intent(Request_Edit.this, AdminLogin.class);
				startActivity(intent);
				finish();
			}
		});

	}

	@Override
	public void onClick(View v) {
		ParkingCommon.getinstance(Request_Edit.this).isAdmin(false);
		if (v.getId() == R.id.timeselect) {
			popupbtn.setBackgroundResource(R.drawable.statechangetime);
			editbtn.setBackgroundResource(R.drawable.custom_border);
			isAdmin();
			checkIfCarRegistered() ;
			//Intent in = new Intent(Request_Edit.this, PopupScreen.class);
			//startActivity(in);
		} else if (v.getId() == R.id.edit) {
			Intent in1 = new Intent(getApplicationContext(),
					CustomerLogin.class);
			in1.putExtra("optn", 1);
			startActivity(in1);
			popupbtn.setBackgroundResource(R.drawable.custom_border);
			editbtn.setBackgroundResource(R.drawable.state_change_white);
		}

	}

	public void isAdmin() {
		ParseQuery<ParseObject> query = ParseQuery
				.getQuery(Prefs.table_adminDeviceID);
		query.whereEqualTo(Prefs.table_col_deviceid, deviceId);
		query.getFirstInBackground(new GetCallback<ParseObject>() {
			@Override
			public void done(ParseObject object, ParseException e) {
				if (object != null) {
					object.deleteInBackground();

				}
			}
		});
	}

	public void checkIfCarRegistered() {
		isInternetPresent = cd.isConnection();
		if (isInternetPresent) {
			final ProgressDialog progressDialog = new ProgressDialog(
					Request_Edit.this);
			progressDialog.setMessage("Por favor,espera.....");
			progressDialog.setIndeterminate(true);
			progressDialog.setCancelable(false);
			progressDialog.show();

			SharedPreferences sharedPreferences = getSharedPreferences(
					Prefs.local_cust_table, Context.MODE_PRIVATE);
			final String carid = sharedPreferences.getString(
					Prefs.local_col_carid, "");

			ParseQuery<ParseObject> query = ParseQuery
					.getQuery(Prefs.table_registedcar);
			query.whereEqualTo(Prefs.table_col_carid, carid);
			query.getFirstInBackground(new GetCallback<ParseObject>() {

				@Override
				public void done(ParseObject object, ParseException e) {
					if (object != null) {
						Intent in = new Intent(Request_Edit.this,
								TimeSelectScreen.class);
						startActivity(in);
					
						progressDialog.dismiss();
					} else {
						progressDialog.dismiss();
						Intent in = new Intent(Request_Edit.this,
								PopUpForCorrectId.class);
						startActivity(in);
						

					}

				}
			});

		} else {

			Toast.makeText(Request_Edit.this,
					getResources().getString(R.string.no_internet),// msg for no
					// internet
					// connection
					Toast.LENGTH_LONG).show();
		}
	}

}
