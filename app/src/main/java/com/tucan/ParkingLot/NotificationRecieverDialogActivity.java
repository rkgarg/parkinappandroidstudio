package com.tucan.ParkingLot;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.os.Bundle;

public class NotificationRecieverDialogActivity extends Activity {

	int alarmNo;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_alarm_reciever_dialog);
		
		showAlertDialog(getIntent().getExtras().getString("notificationMessage"));
	}

	public void showAlertDialog(String message) {
		AlertDialog.Builder builder = new AlertDialog.Builder(this);
		//builder.setTitle("Alert!");
		builder.setMessage(message);
		builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {

			public void onClick(DialogInterface arg0, int arg1) {
				// do something when the OK button is clicked
				finish();
			}
		});
		AlertDialog alert = builder.create();
		alert.show();
	}

}
