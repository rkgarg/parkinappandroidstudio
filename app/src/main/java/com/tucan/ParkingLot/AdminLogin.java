package com.tucan.ParkingLot;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;

import android.text.Editable;
import android.text.TextWatcher;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.parse.GetCallback;
import com.parse.ParseException;
import com.parse.ParseObject;
import com.parse.ParseQuery;

public class AdminLogin extends Activity implements OnClickListener {
	Boolean isInternetPresent = false;
	CheckInternetConnection cd;
	EditText editText1, editText2;
	String deviceid;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
				WindowManager.LayoutParams.FLAG_FULLSCREEN);
		super.onCreate(savedInstanceState);
		setContentView(R.layout.adminlogin);
		if (ParkingCommon.getinstance(this).getAdminLogin() == true) {
			Intent in = new Intent(AdminLogin.this, Registration.class);
			startActivity(in);
			ParkingCommon.getinstance(AdminLogin.this).isAdmin(true);
			AdminLogin.this.finish();
		}
		cd = new CheckInternetConnection(getApplicationContext());
		editText1 = (EditText) findViewById(R.id.usuarioEditText);
		editText2 = (EditText) findViewById(R.id.contrasenaEditText);
		deviceid = "user"
				+ ParkingCommon.getinstance(AdminLogin.this).getDeviceID();
		getDataofdAdmin();
		TextView button1 = (TextView) findViewById(R.id.Guardar);
		button1.setOnClickListener(this);
		if (editText1.getText().toString().compareTo("") != 0) {
			(findViewById(R.id.usuario)).setAlpha(0.5f);
		} else {
			(findViewById(R.id.usuario)).setAlpha(1.0f);
		}
		if (editText2.getText().toString().compareTo("") != 0) {
			(findViewById(R.id.contrasena)).setAlpha(0.5f);
		} else {
			(findViewById(R.id.contrasena)).setAlpha(1.0f);
		}
		editText1.addTextChangedListener(new TextWatcher() {
			@Override
			public void beforeTextChanged(CharSequence s, int start, int count,
					int after) {
			}

			@Override
			public void onTextChanged(CharSequence s, int start, int before,
					int count) {
			}

			@Override
			public void afterTextChanged(Editable s) {
				(findViewById(R.id.usuario)).setAlpha(0.5f);
			}
		});
		editText2.addTextChangedListener(new TextWatcher() {
			@Override
			public void beforeTextChanged(CharSequence s, int start, int count,
					int after) {
			}

			@Override
			public void onTextChanged(CharSequence s, int start, int before,
					int count) {
			}

			@Override
			public void afterTextChanged(Editable s) {
				(findViewById(R.id.contrasena)).setAlpha(0.5f);
			}
		});

	}

	@Override
	public void onClick(View v) {

		if (v.getId() == R.id.Guardar) {
			if (errormessage()) {
				final String name = editText1.getText().toString().trim();
				final String password = editText2.getText().toString().trim();
				isInternetPresent = cd.isConnection();
				if (isInternetPresent) {
					final ProgressDialog progressDialog = new ProgressDialog(
							AdminLogin.this);
					progressDialog.setMessage("Cargando...");
					progressDialog.setIndeterminate(true);
					progressDialog.setCancelable(false);
					progressDialog.show();

					ParseQuery<ParseObject> query = ParseQuery
							.getQuery(Prefs.tableAdmin);

					query.whereEqualTo(Prefs.admin_col_name, name);
					query.whereEqualTo(Prefs.password_col_name, password);

					query.getFirstInBackground(new GetCallback<ParseObject>() {

						@Override
						public void done(ParseObject object, ParseException e) {

							if (editText1.length() != 0
									&& editText2.length() != 0) {
								if (object != null) {
									ParseQuery<ParseObject> queryDeviceid = ParseQuery
											.getQuery("AdminDeviceID");
									queryDeviceid.whereEqualTo("DeviceID",
											deviceid);
									queryDeviceid
											.getFirstInBackground(new GetCallback<ParseObject>() {

												@Override
												public void done(
														ParseObject object,
														ParseException e) {
													if (object == null) {
														ParseObject obj = new ParseObject(
																"AdminDeviceID");

														obj.put("DeviceID",
																deviceid);
														obj.saveInBackground();

														ParkingCommon
																.getinstance(
																		AdminLogin.this)
																.setDataofAdmin(
																		name,
																		password);
														progressDialog
																.dismiss();
														ParkingCommon
																.getinstance(
																		AdminLogin.this)
																.setAdminLogin(
																		true);
														ParkingCommon
																.getinstance(
																		AdminLogin.this)
																.isAdmin(true);
														Intent in = new Intent(
																AdminLogin.this,
																Registration.class);
														startActivity(in);

														AdminLogin.this
																.finish();

													} else {
														ParkingCommon
																.getinstance(
																		AdminLogin.this)
																.setDataofAdmin(
																		name,
																		password);
														progressDialog
																.dismiss();
														ParkingCommon
																.getinstance(
																		AdminLogin.this)
																.setAdminLogin(
																		true);
														Intent in = new Intent(
																AdminLogin.this,
																Registration.class);

														startActivity(in);
														ParkingCommon
																.getinstance(
																		AdminLogin.this)
																.isAdmin(true);
														AdminLogin.this
																.finish();
													}

												}
											});

								} else {
									progressDialog.dismiss();
									ParkingCommon.getinstance(AdminLogin.this)
											.InValidAdmin();
								}
							}
						}
					});
				} else {
					ParkingCommon.getinstance(AdminLogin.this)
							.IsInternetPresent();
				}
			}
		}
	}

	public Boolean errormessage() {
		if (editText1.getText().toString().trim().length() == 0) {
			editText1.setError(getResources().getString(R.string.por_favor));
			return false;
		}
		if (editText2.length() == 0) {
			editText2.setError(getResources()
					.getString(R.string.por_favor_user));
			return false;
		}
		return true;

	}

	public void getDataofdAdmin() {
		SharedPreferences sharedPreferences = getSharedPreferences(
				Prefs.local_cust_table, Context.MODE_PRIVATE);
		String name1 = sharedPreferences.getString(Prefs.Adminname, "");
		String password1 = sharedPreferences.getString(Prefs.Adminpass, "");
		editText1.setText(name1);
		editText2.setText(password1);

	}

}
