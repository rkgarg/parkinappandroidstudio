package com.tucan.ParkingLot;

import java.util.Iterator;
import java.util.List;

import org.json.JSONException;
import org.json.JSONObject;

import android.app.ActivityManager;
import android.app.ActivityManager.RunningTaskInfo;
import android.app.AlertDialog;
import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.ComponentName;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;
import android.support.v4.app.NotificationCompat;
import android.util.Log;

import com.parse.ParsePushBroadcastReceiver;

public class NotificationReceiver extends ParsePushBroadcastReceiver {
	private NotificationManager mNotificationManager;
	String notificationId = "notificationId";
	Notification myNotification;
	CharSequence notificationTitle;
	CharSequence notificationText;
	int badgeCount;
	Context context;
	public int numMessages = 0;
	Boolean Admin;

	public NotificationReceiver(Context context) {
		super();
		this.context = context;
	}

	public NotificationReceiver() {
		super();
	}

	@Override
	public void onReceive(Context context, Intent intent) {

		SharedPreferences sharedPreferences = context.getSharedPreferences(
				Prefs.local_cust_table, Context.MODE_PRIVATE);
		Admin = sharedPreferences.getBoolean("IsAdmin", false);

		// TODO Auto-generated method stub
		SharedPreferences preference = context.getSharedPreferences(
				"MyPREFERENCES", Context.MODE_PRIVATE);
		numMessages = ParkingCommon.getinstance(context).getNotificationCount();
		Log.v("notification count", " "+numMessages);
		myNotification = new NotificationCompat.Builder(context).setTicker("")
				.setWhen(System.currentTimeMillis())
				.setDefaults(Notification.DEFAULT_SOUND).setAutoCancel(true)
				.setSmallIcon(R.drawable.ic_launcher)
				.setDefaults(Notification.DEFAULT_VIBRATE).build();

		try {
			String action = intent.getAction();
			String channel = intent.getExtras().getString("com.parse.Channel");
			JSONObject json = new JSONObject(intent.getExtras().getString(
					"com.parse.Data"));

			Log.d("json", "got action " + action + " on channel " + channel
					+ " with:");
			Iterator itr = json.keys();
			while (itr.hasNext()) {
				String key = (String) itr.next();
				Log.d("json", "..." + key + " => " + json.getString(key));
				notificationTitle = "Oviedo";
				notificationText = json.getString("alert");
			}
		} catch (JSONException e) {
			Log.d("json", "JSONException: " + e.getMessage());
		}
		if (notificationText != null) {
			if (Admin == false) {
				Intent notificationIntent;
				if (notificationText.equals(context.getResources().getString(
						R.string.isReadyNotificationText))) {
					notificationIntent = new Intent(context, Splash.class);
				} else {
					notificationIntent = new Intent(context, Feedback.class);
				}

				notificationIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
				PendingIntent contentIntent = PendingIntent.getActivity(context, 1, notificationIntent, 0);
				myNotification.sound = Uri.parse("android.resource://com.tucan.ParkingLot/raw/client_sound");
				myNotification.setLatestEventInfo(context, notificationTitle,notificationText, contentIntent);
				if (isApplicationSentToBackgroundUser(context)) {
					mNotificationManager = (NotificationManager) context
							.getSystemService(Context.NOTIFICATION_SERVICE);
					mNotificationManager.notify(numMessages, myNotification);
					badgeCount = preference.getInt("badgeNo", -1);
					badgeCount = badgeCount + 1;
					if (numMessages > 1000) {
						numMessages = 0;
					} else {
						numMessages = numMessages + 1;
					}
					ParkingCommon.getinstance(context).setNotificationCount(
							numMessages);
				} else {

					if (notificationText.equals(context.getResources()
							.getString(R.string.isReadyNotificationText))) {
						Intent dialogIntent = new Intent(context, NotificationRecieverDialogActivity.class);
						dialogIntent.putExtra("notificationMessage", notificationText);
						dialogIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
						context.startActivity(dialogIntent);
					} else {
						Log.v("receive", "subscribedChannels");
						Intent dialogIntent = new Intent(context,
								Feedback.class);
						dialogIntent.putExtra("notificationMessage",
								notificationText);
						 dialogIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
						context.startActivity(dialogIntent);
					}
				}
			} else {
				Intent notificationIntent = new Intent(context, RegisteredCars.class);
				notificationIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
				PendingIntent contentIntent = PendingIntent.getActivity(context, 1, notificationIntent, 0);
				myNotification.sound = Uri.parse("android.resource://com.tucan.ParkingLot/raw/admin_sound");
				myNotification.setLatestEventInfo(context, notificationTitle,notificationText, contentIntent);
				if (isApplicationSentToBackground(context)) {
					mNotificationManager = (NotificationManager) context
							.getSystemService(Context.NOTIFICATION_SERVICE);
					mNotificationManager.notify(numMessages, myNotification);
					badgeCount = preference.getInt("badgeNo", -1);
					badgeCount = badgeCount + 1;
					if (numMessages > 1000) {
						numMessages = 0;
					} else {
						numMessages = numMessages + 1;
					}
					ParkingCommon.getinstance(context).setNotificationCount(
							numMessages);
				} else {
					mNotificationManager = (NotificationManager) context
							.getSystemService(Context.NOTIFICATION_SERVICE);
					mNotificationManager.notify(numMessages, myNotification);
					badgeCount = preference.getInt("badgeNo", -1);
					badgeCount = badgeCount + 1;
					if (numMessages > 1000) {
						numMessages = 0;
					} else {
						numMessages = numMessages + 1;
					}
					ParkingCommon.getinstance(context).setNotificationCount(
							numMessages);
				}
			}
		}

	}

	private boolean isApplicationSentToBackground(Context mcontext) {
		// TODO Auto-generated method stub
		ActivityManager am = (ActivityManager) mcontext
				.getSystemService(Context.ACTIVITY_SERVICE);
		List<RunningTaskInfo> tasks = am.getRunningTasks(1);
		if (!tasks.isEmpty()) {
			ComponentName topActivity = tasks.get(0).topActivity;
			if (topActivity.getClassName().equals("com.tucan.ParkingLot.RegisteredCars")) {
				ParkingCommon.getinstance(mcontext).getRefrernce()
						.refreshList();
				return true;
			}

		}
		return false;
	}

	private boolean isApplicationSentToBackgroundUser(Context mcontext) {
		// TODO Auto-generated method stub
		ActivityManager am = (ActivityManager) mcontext
				.getSystemService(Context.ACTIVITY_SERVICE);
		List<RunningTaskInfo> tasks = am.getRunningTasks(1);
		if (!tasks.isEmpty()) {
			ComponentName topActivity = tasks.get(0).topActivity;
			if (!topActivity.getPackageName().equals(mcontext.getPackageName())) {
				return true;
			}
		}
		return false;
	}

}