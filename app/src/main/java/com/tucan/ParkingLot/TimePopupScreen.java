package com.tucan.ParkingLot;

import android.app.Activity;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.LinearLayout;

public class TimePopupScreen extends Activity {
	ImageButton okimgbtn;
	ImageButton close;

	@Override
	protected void onCreate(Bundle savedInstanceState) {

		super.onCreate(savedInstanceState);
		setContentView(R.layout.timepopupscreen);

		// Getting info of which image to show on pop screen
		int a = getIntent().getExtras().getInt("img");

		close = (ImageButton) findViewById(R.id.button1);

		okimgbtn = (ImageButton) findViewById(R.id.button2);

		if (a == 1)
			okimgbtn.setImageResource(R.drawable.time15);

		else if (a == 2)
			okimgbtn.setImageResource(R.drawable.time20);

		else if (a == 3)
			okimgbtn.setImageResource(R.drawable.time25);

		else if (a == 4)
			okimgbtn.setImageResource(R.drawable.time30);

		if (a == 5)
			okimgbtn.setImageResource(R.drawable.time35);

		else if (a == 6)
			okimgbtn.setImageResource(R.drawable.time40);

		else if (a == 7)
			okimgbtn.setImageResource(R.drawable.time45);

		close.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				finish();

			}
		});
		(findViewById(R.id.button3)).setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				finish();
			}
		});
		(findViewById(R.id.time_popup_layout))
				.setOnClickListener(new OnClickListener() {

					@Override
					public void onClick(View v) {
						// TODO Auto-generated method stub
						finish();
					}
				});
		okimgbtn.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				finish();
			}
		});
	}

}
