package com.tucan.ParkingLot;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.view.WindowManager;
import android.view.inputmethod.EditorInfo;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.TextView;

import com.parse.GetCallback;
import com.parse.ParseException;
import com.parse.ParseObject;
import com.parse.ParseQuery;
import com.parse.SaveCallback;

public class CustomerLogin extends Activity implements OnClickListener {
	Boolean isInternetPresent = false;
	CheckInternetConnection cd;
	int optn = 0;
	ImageButton oivedo;
	MyTextView accept;
	EditText name, id, carid, email, phone;
	String stringname, stringid, stringcarid, stringemail, stringphone;
	String deviceId;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		//getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
				//WindowManager.LayoutParams.FLAG_FULLSCREEN);

		super.onCreate(savedInstanceState);
		setContentView(R.layout.customerlogin);

		cd = new CheckInternetConnection(getApplicationContext());

		name = (EditText) findViewById(R.id.name);
		id = (EditText) findViewById(R.id.id);
		carid = (EditText) findViewById(R.id.carid);
		email = (EditText) findViewById(R.id.email);
		phone = (EditText) findViewById(R.id.phone);
		oivedo = (ImageButton) findViewById(R.id.button1);
		(findViewById(R.id.name_layout))
				.setOnClickListener(new OnClickListener() {

					@Override
					public void onClick(View v) {
						// TODO Auto-generated method stub
						name.requestFocus();
					}
				});
		(findViewById(R.id.back)).setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				onBackPressed();
			}
		});
		(findViewById(R.id.phone_layout))
				.setOnClickListener(new OnClickListener() {

					@Override
					public void onClick(View v) {
						// TODO Auto-generated method stub
						phone.requestFocus();
					}
				});
		(findViewById(R.id.id_layout))
				.setOnClickListener(new OnClickListener() {

					@Override
					public void onClick(View v) {
						// TODO Auto-generated method stub
						id.requestFocus();
					}
				});
		(findViewById(R.id.carid_layout))
				.setOnClickListener(new OnClickListener() {

					@Override
					public void onClick(View v) {
						// TODO Auto-generated method stub
						carid.requestFocus();
					}
				});
		(findViewById(R.id.email_layout))
				.setOnClickListener(new OnClickListener() {

					@Override
					public void onClick(View v) {
						// TODO Auto-generated method stub
						email.requestFocus();
					}
				});
		oivedo.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View arg0) {
				Intent intent = new Intent(CustomerLogin.this, AdminLogin.class);
				startActivity(intent);
				finish();
			}
		});
		toShowDataOnEditScreen();
		optn = getIntent().getExtras().getInt("optn");
		// accepting user info
		accept = (MyTextView) findViewById(R.id.guardar);
		accept.setOnClickListener(this);

		deviceId = "user"
				+ ParkingCommon.getinstance(CustomerLogin.this).getDeviceID();
		if (name.getText().toString().compareTo("") != 0) {
			(findViewById(R.id.textView1)).setAlpha(0.5f);
		} else {
			(findViewById(R.id.textView1)).setAlpha(1.0f);
		}
		if (id.getText().toString().compareTo("") != 0) {
			(findViewById(R.id.textView2)).setAlpha(0.5f);
		} else {
			(findViewById(R.id.textView2)).setAlpha(1.0f);
		}
		if (carid.getText().toString().compareTo("") != 0) {
			(findViewById(R.id.textView3)).setAlpha(0.5f);
		} else {
			(findViewById(R.id.textView3)).setAlpha(1.0f);
		}
		if (email.getText().toString().compareTo("") != 0) {
			(findViewById(R.id.textView4)).setAlpha(0.5f);
		} else {
			(findViewById(R.id.textView4)).setAlpha(1.0f);
		}
		if (phone.getText().toString().compareTo("") != 0) {
			(findViewById(R.id.textView5)).setAlpha(0.5f);
		} else {
			(findViewById(R.id.textView5)).setAlpha(1.0f);
		}
		name.addTextChangedListener(new TextWatcher() {
			@Override
			public void beforeTextChanged(CharSequence s, int start, int count,
					int after) {
			}

			@Override
			public void onTextChanged(CharSequence s, int start, int before,
					int count) {
			}

			@Override
			public void afterTextChanged(Editable s) {
				(findViewById(R.id.textView1)).setAlpha(0.3f);
			}
		});
		phone.addTextChangedListener(new TextWatcher() {
			@Override
			public void beforeTextChanged(CharSequence s, int start, int count,
					int after) {
			}

			@Override
			public void onTextChanged(CharSequence s, int start, int before,
					int count) {
			}

			@Override
			public void afterTextChanged(Editable s) {
				(findViewById(R.id.textView5)).setAlpha(0.3f);
			}
		});
		id.addTextChangedListener(new TextWatcher() {
			@Override
			public void beforeTextChanged(CharSequence s, int start, int count,
					int after) {
			}

			@Override
			public void onTextChanged(CharSequence s, int start, int before,
					int count) {
			}

			@Override
			public void afterTextChanged(Editable s) {
				(findViewById(R.id.textView2)).setAlpha(0.3f);
			}
		});
		carid.addTextChangedListener(new TextWatcher() {
			@Override
			public void beforeTextChanged(CharSequence s, int start, int count,
					int after) {
			}

			@Override
			public void onTextChanged(CharSequence s, int start, int before,
					int count) {
			}

			@Override
			public void afterTextChanged(Editable s) {
				(findViewById(R.id.textView3)).setAlpha(0.5f);
			}
		});
		email.addTextChangedListener(new TextWatcher() {
			@Override
			public void beforeTextChanged(CharSequence s, int start, int count,
					int after) {
			}

			@Override
			public void onTextChanged(CharSequence s, int start, int before,
					int count) {
			}

			@Override
			public void afterTextChanged(Editable s) {
				(findViewById(R.id.textView4)).setAlpha(0.5f);
			}
		});

		if (optn == 2) {
			carid.requestFocus();
			carid.setImeOptions(EditorInfo.IME_ACTION_DONE);
		}

	}

	// method created for one time login using local database
	public void custLogin(View v) {
		SharedPreferences sharedPreferences = getSharedPreferences(
				Prefs.local_cust_table, Context.MODE_PRIVATE);
		SharedPreferences.Editor editor = sharedPreferences.edit();

		editor.putBoolean(Prefs.local_cust_islogin, true);
		editor.commit();

	}

	@Override
	public void onClick(View v) {
		// for loading
		stringname = name.getText().toString().trim();
		stringid = id.getText().toString().trim();
		stringcarid = ParkingCommon.getinstance(CustomerLogin.this).returnCarId(carid.getText().toString().trim().toUpperCase());
		stringemail = email.getText().toString().trim();
		stringphone = phone.getText().toString().trim();

		ParkingCommon.getinstance(CustomerLogin.this).setAllInfo(stringname,
				stringid, stringcarid, stringemail, stringphone, deviceId);

		if (errormessage() == true) {

			if (isEmailValid(stringemail)) {
				isInternetPresent = cd.isConnection();
				if (isInternetPresent) {

					custLogin(v);

					if (optn == 0) {
						newEntry();
						ParkingCommon.getinstance(CustomerLogin.this).isAdmin(
								false);

					} else if (optn == 1) {
						updateEnrty();

					} else if (optn == 2) {
						changeCarId();
					}
				} else {

					ParkingCommon.getinstance(CustomerLogin.this)
							.okAlertDialog(
									getResources().getString(
											R.string.no_internet));
				}
			} else
				email.setError(getResources().getString(
						R.string.enter_correct_email));
		}

	}

	public boolean errormessage() {
		if (stringname.length() == 0) {
			name.setError(getResources().getString(R.string.enter_name));
			return false;
		} else if (stringid.length() == 0) {
			id.setError(getResources().getString(R.string.enter_id));
			return false;
		} else if (stringcarid.length() == 0) {
			carid.setError(getResources().getString(R.string.enter_car_id));
			return false;
		} else if (stringemail.length() == 0) {
			email.setError(getResources().getString(R.string.enter_email));
			return false;
		} else if (stringphone.length() == 0) {
			phone.setError(getResources().getString(R.string.enter_phone_no));
			return false;
		}
		return true;
	}

	public static boolean isEmailValid(String email) {
		boolean isValid = false;

		String expression = "^[\\w\\.-]+@([\\w\\-]+\\.)+[A-Z]{2,4}$";
		CharSequence inputStr = email;

		Pattern pattern = Pattern.compile(expression, Pattern.CASE_INSENSITIVE);
		Matcher matcher = pattern.matcher(inputStr);
		if (matcher.matches()) {
			isValid = true;
		}
		return isValid;
	}

	public void toShowDataOnEditScreen() {
		SharedPreferences sharedPreferences = getSharedPreferences(
				Prefs.local_cust_table, Context.MODE_PRIVATE);
		String namec = sharedPreferences.getString(Prefs.local_col_name, "");
		String idc = sharedPreferences.getString(Prefs.local_col_id, "");
		String caridc = sharedPreferences.getString(Prefs.local_col_carid, "");
		String emailc = sharedPreferences.getString(Prefs.loacl_col_email, "");
		String phonec = sharedPreferences.getString(Prefs.local_col_phone, "");
		name.setText(namec);
		id.setText(idc);
		carid.setText(caridc);
		email.setText(emailc);
		phone.setText(phonec);
	}

	public void newEntry() {
		final ProgressDialog progressDialog = new ProgressDialog(
				CustomerLogin.this);
		progressDialog.setMessage("Por favor,espera.....");
		progressDialog.setIndeterminate(true);
		progressDialog.setCancelable(false);
		progressDialog.show();

		ParseQuery<ParseObject> query = ParseQuery
				.getQuery(Prefs.table_userdata);
		query.whereEqualTo(Prefs.table_col_deviceid, deviceId);
		query.getFirstInBackground(new GetCallback<ParseObject>() {
			@Override
			public void done(ParseObject object, ParseException e) {

				if (object == null) {
					isAdmin();
					ParseObject pasobj = new ParseObject(Prefs.table_userdata);
					pasobj.put(Prefs.table_col_name, stringname);
					pasobj.put(Prefs.table_col_id, stringid);
					pasobj.put(Prefs.table_col_carid, stringcarid);
					pasobj.put(Prefs.table_col_email, stringemail);
					pasobj.put(Prefs.table_col_phone, stringphone);
					pasobj.put(Prefs.table_col_deviceid, deviceId);
					pasobj.put(Prefs.device_id, deviceId);
					pasobj.saveInBackground(new SaveCallback() {

						@Override
						public void done(ParseException e) {
							ParkingCommon.getinstance(CustomerLogin.this)
									.setCarId(stringcarid);
							ParkingCommon.getinstance(CustomerLogin.this)
									.setEmail(stringemail);
							progressDialog.dismiss();
							Intent in = new Intent(CustomerLogin.this,
									Request_Edit.class);
							startActivity(in);
							CustomerLogin.this.finish();

						}
					});

				}

				else {
					isAdmin();
					object.put(Prefs.table_col_name, name.getText().toString());
					object.put(Prefs.table_col_id, id.getText().toString());
					object.put(Prefs.table_col_carid, stringcarid);
					object.put(Prefs.table_col_email, email.getText()
							.toString());
					object.put(Prefs.table_col_phone, phone.getText()
							.toString());
					object.saveInBackground(new SaveCallback() {

						@Override
						public void done(ParseException e) {
							progressDialog.dismiss();
							 ParkingCommon.getinstance(CustomerLogin.this).setEmail(email.getText()
										.toString());
							Intent in = new Intent(CustomerLogin.this,
									Request_Edit.class);
							startActivity(in);
							CustomerLogin.this.finish();
						}
					});

				}
			}
		});
	}

	public void isAdmin() {
		ParseQuery<ParseObject> query = ParseQuery
				.getQuery(Prefs.table_adminDeviceID);
		query.whereEqualTo(Prefs.table_col_deviceid, deviceId);
		query.getFirstInBackground(new GetCallback<ParseObject>() {
			@Override
			public void done(ParseObject object, ParseException e) {
				if (object != null) {
					object.deleteInBackground();

				}
			}
		});
	}

	public void updateEnrty() {
		final ProgressDialog progressDialog = new ProgressDialog(
				CustomerLogin.this);
		progressDialog.setMessage("Por favor,espera.....");
		progressDialog.setIndeterminate(true);
		progressDialog.setCancelable(false);
		progressDialog.show();
		ParseQuery<ParseObject> updatequery = ParseQuery
				.getQuery(Prefs.table_userdata);
		updatequery.whereEqualTo(Prefs.table_col_deviceid, deviceId);
		updatequery.getFirstInBackground(new GetCallback<ParseObject>() {

			@Override
			public void done(ParseObject object, ParseException e) {
				if (object != null) {
					object.put(Prefs.table_col_name, name.getText().toString());
					object.put(Prefs.table_col_id, id.getText().toString());
					object.put(Prefs.table_col_carid, stringcarid);
					object.put(Prefs.table_col_email, email.getText()
							.toString());
					object.put(Prefs.table_col_phone, phone.getText()
							.toString());
					object.saveInBackground(new SaveCallback() {

						@Override
						public void done(ParseException e) {
							ParkingCommon.getinstance(CustomerLogin.this)
									.okAlertDialog(
											getResources().getString(
													R.string.sus_datos));
							progressDialog.dismiss();
							CustomerLogin.this.finish();
						}
					});
				}

			}
		});
	}

	public void changeCarId() {
		final ProgressDialog progressDialog = new ProgressDialog(
				CustomerLogin.this);
		progressDialog.setMessage("Por favor,espera.....");
		progressDialog.setIndeterminate(true);
		progressDialog.setCancelable(false);
		progressDialog.show();
		ParseQuery<ParseObject> updatequery = ParseQuery
				.getQuery(Prefs.table_userdata);
		updatequery.whereEqualTo(Prefs.table_col_deviceid, deviceId);
		updatequery.getFirstInBackground(new GetCallback<ParseObject>() {

			@Override
			public void done(ParseObject object, ParseException e) {
				if (object != null) {
					object.put(Prefs.table_col_carid, carid.getText()
							.toString());
					object.saveInBackground(new SaveCallback() {

						@Override
						public void done(ParseException e) {
							ParkingCommon.getinstance(CustomerLogin.this)
									.okAlertDialog(
											getResources().getString(
													R.string.ha_cambiado));
							progressDialog.dismiss();
							CustomerLogin.this.finish();
						}
					});
				}

			}
		});
	}
}
