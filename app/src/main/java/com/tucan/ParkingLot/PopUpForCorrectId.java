package com.tucan.ParkingLot;



import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.ImageButton;

public class PopUpForCorrectId extends Activity implements OnClickListener {
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.popupforcorrectid);

		ImageButton continu = (ImageButton) findViewById(R.id.continu);
		continu.setOnClickListener(this);
		ImageButton cancel = (ImageButton) findViewById(R.id.cancel);
		cancel.setOnClickListener(this);
		ImageButton cross = (ImageButton) findViewById(R.id.button1);
		cross.setOnClickListener(this);
	}

	@Override
	public void onClick(View v) {

		if (v.getId() == R.id.continu) {
			Intent in = new Intent(getApplicationContext(), ChangeCarIdActivity.class);
			startActivity(in);
			PopUpForCorrectId.this.finish();
		} else if (v.getId() == R.id.cancel || v.getId() == R.id.button1) {
			PopUpForCorrectId.this.finish();
		}

	}

}
