package com.tucan.ParkingLot;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import android.R.color;
import android.app.Activity;
import android.app.ProgressDialog;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v4.widget.SwipeRefreshLayout.OnRefreshListener;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ListView;
import android.widget.Toast;

import com.parse.FindCallback;
import com.parse.ParseException;
import com.parse.ParseObject;
import com.parse.ParseQuery;

public class RegisteredCars extends Activity implements OnClickListener {
	Boolean IsInternetPresent = false;
	ArrayList<CarList> carIDArrayList = new ArrayList<CarList>();
	ProgressDialog progressDialog = null;
	ListView listview;
	CheckInternetConnection cd;
	private SwipeRefreshLayout mSwipeRefreshLayout;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
				WindowManager.LayoutParams.FLAG_FULLSCREEN);
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.registered_cars);
		cd = new CheckInternetConnection(getApplicationContext());
		ParkingCommon.getinstance(RegisteredCars.this).setRefrernce(this);
		ImageButton registerBackBtn = (ImageButton) findViewById(R.id.backButton);
		registerBackBtn.setOnClickListener(this);
		mSwipeRefreshLayout = (SwipeRefreshLayout) findViewById(R.id.container);
		mSwipeRefreshLayout.setColorScheme(color.white, color.white,
				color.white, color.white);
		mSwipeRefreshLayout.setOnRefreshListener(new OnRefreshListener() {

			@Override
			public void onRefresh() {
				IsInternetPresent = cd.isConnection();
				if (IsInternetPresent == true) {
					mSwipeRefreshLayout.setRefreshing(true);
					fetchRegisteredCar();
				} else {
					mSwipeRefreshLayout.setRefreshing(false);
					ParkingCommon.getinstance(RegisteredCars.this)
							.IsInternetPresent();
				}
			}
		});
		listview = (ListView) findViewById(R.id.RegistedCarList);

		progressDialog = new ProgressDialog(RegisteredCars.this);
		progressDialog.setMessage("Cargando...");
		progressDialog.setIndeterminate(true);
		progressDialog.setCancelable(false);
		progressDialog.show();

		fetchRegisteredCar();

	}

	private void fetchRegisteredCar() {

		ParseQuery<ParseObject> query = ParseQuery.getQuery("RegistedCar");
		query.whereExists("CarID");
		query.whereExists("IsReady");
		query.whereExists("IsDeliver");

		query.setLimit(1000);
		query.findInBackground(new FindCallback<ParseObject>() {

			private Date date1, date2;
			long difference;

			@Override
			public void done(List<ParseObject> list, ParseException e) {
				if (e == null) {
					if (list.size() > 0) {
						mSwipeRefreshLayout.setRefreshing(false);
						carIDArrayList.clear();

						Calendar c = Calendar.getInstance();
						SimpleDateFormat sdf = new SimpleDateFormat(
								"yyyy-MM-dd HH:mm:ss");
						final String strDate = sdf.format(c.getTime());
						for (int i = 0; i < list.size(); i++) {
							ParseObject obj = list.get(i);
							String carid = obj.getString("CarID");
							Boolean ready = obj.getBoolean("IsReady");
							Boolean deliver = obj.getBoolean("IsDeliver");
							String userTime = obj.getString("TimeSelected");
							if (!userTime.equals("")) {
								CarList objectcarList = new CarList(carid,
										ready, deliver);
								try {
									date1 = sdf.parse(userTime);
									date2 = sdf.parse(strDate);

									System.out.println(date1.toString());
								} catch (java.text.ParseException e1) {
									// TODO Auto-generated catch block
									e1.printStackTrace();
								}

								if (date1.compareTo(date2) <= 0) {
									carIDArrayList.add(objectcarList);
								}
							}
						}
						setCarAdapter();
					} else {
						setCarAdapter();
						progressDialog.dismiss();
						mSwipeRefreshLayout.setRefreshing(false);
						ParkingCommon.getinstance(RegisteredCars.this)
								.NoRegistedCar();
					}
				}

			}
		});
	}

	@Override
	public void onClick(View v) {
		if (v.getId() == R.id.backButton) {

			RegisteredCars.this.finish();
		}
	}

	public void setCarAdapter() {
		progressDialog.dismiss();
		RegistedCarAdapter objectAdapter = new RegistedCarAdapter(this,
				carIDArrayList, listview);
		listview.setAdapter(objectAdapter);

	}

	public void refreshList() {

		carIDArrayList.clear();
		fetchRegisteredCar();

	}
}
