package com.tucan.ParkingLot;

import com.parse.gdata.Escaper;

import android.app.Activity;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.view.View.OnClickListener;
import android.widget.ImageButton;

public class ThanksForRating extends Activity {
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
				WindowManager.LayoutParams.FLAG_FULLSCREEN);
		super.onCreate(savedInstanceState);
	    setContentView(R.layout.thanksforrating);
	    
	    MyTextView salir=(MyTextView)findViewById(R.id.salir);
	    salir.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View arg0) {
				finish();
				System.exit(0);
				ThanksForRating.this.finish();
				
			}
		});
	}

}
