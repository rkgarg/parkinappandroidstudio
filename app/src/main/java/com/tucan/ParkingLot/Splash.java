package com.tucan.ParkingLot;





import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.Window;
import android.view.WindowManager;

public class Splash extends Activity {

	Boolean def = false;
	Thread timer;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
				WindowManager.LayoutParams.FLAG_FULLSCREEN);

		super.onCreate(savedInstanceState);
		setContentView(R.layout.splash);
		
	

		// this is used so that user doesn't have login again
		SharedPreferences sharedPreferences = getSharedPreferences(
				Prefs.local_cust_table, Context.MODE_PRIVATE);
		Boolean islogin = sharedPreferences.getBoolean(
				Prefs.local_cust_islogin, def);

		// first time login then this condition will execute
		if (islogin == false) {
			setContentView(R.layout.splash);
			timer = new Thread() {
				public void run() {
					try {
						sleep(3000);
					} catch (InterruptedException e) {
						e.printStackTrace();
					} finally {
						Intent in = new Intent(Splash.this, MainActivity.class);
						startActivity(in);
						Splash.this.finish();
					}
				}
			};
			timer.start();
		}
		// condition in which no need to make id again
		else {
			setContentView(R.layout.splash);
			timer = new Thread() {
				public void run() {
					try {
						sleep(3000);
					} catch (InterruptedException e) {
						e.printStackTrace();
					} finally {
						Intent in1 = new Intent(Splash.this, Request_Edit.class);
						startActivity(in1);
						Splash.this.finish();
					}
				}
			};
			timer.start();

		}
	}

}
