package com.tucan.ParkingLot;

import com.parse.GetCallback;
import com.parse.ParseException;
import com.parse.ParseObject;
import com.parse.ParseQuery;
import com.parse.SaveCallback;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.EditText;

public class ChangeCarIdActivity extends Activity {
EditText carid;
String	deviceId;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_change_car_id);
		carid = (EditText) findViewById(R.id.carid);
		SharedPreferences sharedPreferences = getSharedPreferences(
				Prefs.local_cust_table, Context.MODE_PRIVATE);
		String caridc = sharedPreferences.getString(Prefs.local_col_carid, "");

			deviceId = "user"
				+ ParkingCommon.getinstance(ChangeCarIdActivity.this).getDeviceID();
		carid.setText(caridc);
(findViewById(R.id.back)).setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				finish();
			}
		});
		(findViewById(R.id.guardar)).setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				 changeCarId();
			}
		});
		
	}
	public void changeCarId() {
		final ProgressDialog progressDialog = new ProgressDialog(
				ChangeCarIdActivity.this);
		progressDialog.setMessage("Por favor,espera.....");
		progressDialog.setIndeterminate(true);
		progressDialog.setCancelable(false);
		progressDialog.show();
		ParseQuery<ParseObject> updatequery = ParseQuery
				.getQuery(Prefs.table_userdata);
		updatequery.whereEqualTo(Prefs.table_col_deviceid, deviceId);
		updatequery.getFirstInBackground(new GetCallback<ParseObject>() {

			@Override
			public void done(ParseObject object, ParseException e) {
				if (object != null) {
					object.put(Prefs.table_col_carid,ParkingCommon.getinstance(ChangeCarIdActivity.this).returnCarId(carid.getText().toString().toUpperCase()));
					object.saveInBackground(new SaveCallback() {

						@Override
						public void done(ParseException e) {
							ParkingCommon.getinstance(ChangeCarIdActivity.this)
									.okAlertDialog(
											getResources().getString(
													R.string.ha_cambiado));
							progressDialog.dismiss();
							ParkingCommon.getinstance(ChangeCarIdActivity.this)
							.setCarId(ParkingCommon.getinstance(ChangeCarIdActivity.this).returnCarId(carid.getText().toString().toUpperCase()));
							ChangeCarIdActivity.this.finish();
						}
					});
				}

			}
		});
	}
}
