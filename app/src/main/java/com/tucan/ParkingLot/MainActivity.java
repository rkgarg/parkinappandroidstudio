package com.tucan.ParkingLot;


import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ImageButton;

import java.io.Closeable;
import java.io.IOException;
import java.io.InputStream;

public class MainActivity extends Activity implements OnClickListener {
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        MyTextView termsAndCondition = (MyTextView) findViewById(R.id.termsAndConditionTV);
        String text = readFile(this, "terms_and_condition.txt").toString();
        termsAndCondition.setText(text);

        if (ParkingCommon.getinstance(this).getTermsSelected() == true) {
            Intent in = new Intent(MainActivity.this, CustomerLogin.class);
            in.putExtra("optn", 0);
            startActivity(in);
            MainActivity.this.finish();
        } else {

        }
        buttonCustLogin();
    }

    public void buttonCustLogin() {
        ImageButton customerlogin = (ImageButton) findViewById(R.id.button2);
        customerlogin.setOnClickListener(this);
    }

    private static CharSequence readFile(Activity activity, String fileName) {
        InputStream in = null;
        try {
            in = activity.getAssets().open(fileName);
            int size = in.available();
            byte[] buffer = new byte[size];
            in.read(buffer);
            String text = new String(buffer);
            buffer = null;
            return text;
        } catch (IOException e) {
            return "";
        } finally {
            closeStream(in);
        }
    }

    private static void closeStream(Closeable stream) {
        if (stream != null) {
            try {
                stream.close();
            } catch (IOException e) {
                // Ignore
            }
        }
    }

    @Override
    public void onClick(View v) {
        if (v.getId() == R.id.button2) {
            ParkingCommon.getinstance(MainActivity.this).setTermsSelected(true);
            Intent in = new Intent(MainActivity.this, CustomerLogin.class);
            in.putExtra("optn", 0);
            startActivity(in);
            MainActivity.this.finish();
        }
    }
}
