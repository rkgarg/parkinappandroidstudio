package com.tucan.ParkingLot;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import android.R.color;
import android.app.Activity;
import android.app.ActivityManager;
import android.app.AlarmManager;
import android.app.PendingIntent;
import android.app.ProgressDialog;
import android.content.ComponentName;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ImageButton;
import android.widget.TextView;

import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;
import com.parse.FindCallback;
import com.parse.GetCallback;
import com.parse.ParseException;
import com.parse.ParseInstallation;
import com.parse.ParseObject;
import com.parse.ParsePush;
import com.parse.ParseQuery;
import com.parse.SaveCallback;

public class TimeSelectScreen extends Activity implements OnClickListener {
	ImageButton time15, time20, time25, time30, time35, time40, time45, timeok,
			timecancel;
	String car, deviceId;
	int selectedTime = 0;
	Boolean isInternetPresent = false;
	CheckInternetConnection cd;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
				WindowManager.LayoutParams.FLAG_FULLSCREEN);
		super.onCreate(savedInstanceState);

		setContentView(R.layout.timselectscreen);
		AdView mAdView = (AdView) findViewById(R.id.adView);
		AdRequest adRequest = new AdRequest.Builder().build();
		mAdView.loadAd(adRequest);
		cd = new CheckInternetConnection(getApplicationContext());

		deviceId = "user"
				+ ParkingCommon.getinstance(TimeSelectScreen.this)
						.getDeviceID();

		TextView showcarid = (TextView) findViewById(R.id.textView2);
		car = ParkingCommon.getinstance(TimeSelectScreen.this).getCarId();
		showcarid.setText(car);
		ImageButton backbtn = (ImageButton) findViewById(R.id.button2);
		backbtn.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View arg0) {
				TimeSelectScreen.this.finish();
			}
		});

		ImageButtonTime();

		timeok = (ImageButton) findViewById(R.id.imagebtn8);
		isInternetPresent = cd.isConnection();
		if (isInternetPresent) {
			timeok.setOnClickListener(new OnClickListener() {

				@Override
				public void onClick(View arg0) {

					timeok.setBackgroundResource(R.drawable.select_custom_border_ok);
					timecancel.setBackgroundResource(R.drawable.custom_border);

					if (selectedTime == 15) {
						Intent iin = new Intent(getApplicationContext(),
								TimePopupScreen.class);
						iin.putExtra("img", 1);
						startActivity(iin);
						// finish();

					} else if (selectedTime == 20) {
						Intent in1 = new Intent(getApplicationContext(),
								TimePopupScreen.class);
						in1.putExtra("img", 2);
						startActivity(in1);
						// finish();
					} else if (selectedTime == 25) {
						Intent in2 = new Intent(getApplicationContext(),
								TimePopupScreen.class);
						in2.putExtra("img", 3);
						startActivity(in2);
						// finish();
					} else if (selectedTime == 30) {
						Intent in3 = new Intent(getApplicationContext(),
								TimePopupScreen.class);
						in3.putExtra("img", 4);
						startActivity(in3);
						// finish();
					} else if (selectedTime == 35) {
						Intent in4 = new Intent(getApplicationContext(),
								TimePopupScreen.class);
						in4.putExtra("img", 5);
						startActivity(in4);
						// finish();
					} else if (selectedTime == 40) {
						Intent in5 = new Intent(getApplicationContext(),
								TimePopupScreen.class);
						in5.putExtra("img", 6);
						startActivity(in5);
						// finish();
					} else if (selectedTime == 45) {
						Intent in6 = new Intent(getApplicationContext(),
								TimePopupScreen.class);
						in6.putExtra("img", 7);
						startActivity(in6);
						// finish();
					}

					selecttime(selectedTime);
				}
			});
		} else {
			String message = getResources().getString(R.string.usted_no);
			ParkingCommon.getinstance(TimeSelectScreen.this).okAlertDialog(
					message);
		}
		timecancel = (ImageButton) findViewById(R.id.imagebtn9);
		isInternetPresent = cd.isConnection();
		if (isInternetPresent) {
			timecancel.setOnClickListener(new OnClickListener() {

				@Override
				public void onClick(View v) {

					timecancel
							.setBackgroundResource(R.drawable.select_custom_border_ok);

					timeok.setBackgroundResource(R.drawable.custom_border);

					time15.setBackgroundResource(R.drawable.custom_border);
					time20.setBackgroundResource(R.drawable.custom_border);
					time25.setBackgroundResource(R.drawable.custom_border);
					time30.setBackgroundResource(R.drawable.custom_border);
					time35.setBackgroundResource(R.drawable.custom_border);
					time40.setBackgroundResource(R.drawable.custom_border);
					time45.setBackgroundResource(R.drawable.custom_border);

					final ProgressDialog progressDialog = new ProgressDialog(
							TimeSelectScreen.this);
					progressDialog.setMessage("Cargando.....");
					progressDialog.setIndeterminate(true);
					progressDialog.setCancelable(false);
					progressDialog.show();

					Calendar c = Calendar.getInstance();
					final SimpleDateFormat sdf = new SimpleDateFormat(
							"yyyy-MM-dd HH:mm:ss");
					final String strDate1 = sdf.format(c.getTime());

					ParseQuery<ParseObject> query = ParseQuery
							.getQuery(Prefs.table_registedcar);
					query.whereEqualTo(Prefs.table_col_carid, car);
					query.getFirstInBackground(new GetCallback<ParseObject>() {

						@Override
						public void done(ParseObject object, ParseException e) {
							progressDialog.dismiss();
							if (e == null) {
								if (object != null) {
									Date date1 = null, date2 = null;
									String timeselected = object
											.getString(Prefs.table_col_timeselected);
									if (timeselected.compareTo("") != 0) {
										try {
											date1 = (Date) sdf
													.parse(timeselected);
											date2 = (Date) sdf.parse(strDate1);
										} catch (java.text.ParseException e1) {
											// TODO Auto-generated catch block
											e1.printStackTrace();
										}
										// try {
										Log.v("date1", "" + date1.getDate());
										Log.v("date2", "" + date2.getDate());
										if (date1.compareTo(date2) <= 0) {

											ParkingCommon
													.getinstance(
															TimeSelectScreen.this)
													.okAlertDialog(
															getResources()
																	.getString(
																			R.string.el_tiempo));

										} else {
											ParseQuery<ParseObject> query = ParseQuery
													.getQuery(Prefs.table_registedcar);
											query.whereEqualTo(
													Prefs.table_col_timeselected,
													timeselected);
											query.getFirstInBackground(new GetCallback<ParseObject>() {
												@Override
												public void done(
														ParseObject object,
														ParseException e) {
													if (object != null) {
														object.put(
																Prefs.table_col_timeselected,
																"");
														object.saveInBackground(new SaveCallback() {

															@Override
															public void done(
																	ParseException e) {
																progressDialog
																		.dismiss();
																Intent intent = new Intent(
																		getApplicationContext(),AlarmReceiver.class);
																intent.putExtra("car_id", car);
																PendingIntent pendingIntent = PendingIntent
																		.getBroadcast(getApplicationContext(),1,intent,0);
																AlarmManager alarmManager = (AlarmManager) getSystemService(ALARM_SERVICE);
																alarmManager
																		.cancel(pendingIntent);
																ParkingCommon
																		.getinstance(
																				TimeSelectScreen.this)
																		.okAlertDialog(
																				getResources()
																						.getString(
																								R.string.se_ha));
															}
														});
													}
												}
											});

										}
										// } catch (NullPointerException e1) {
										// e1.printStackTrace();
										// }
									} else {
										ParkingCommon
												.getinstance(
														TimeSelectScreen.this)
												.okAlertDialog(
														getResources()
																.getString(
																		R.string.no_ha));
									}
								}
							} else {
								e.printStackTrace();
							}
						}
					});

				}
			});
		} else {

			ParkingCommon.getinstance(TimeSelectScreen.this).okAlertDialog(
					getResources().getString(R.string.no_internet));
		}
	}

	public void ImageButtonTime() {
		time15 = (ImageButton) findViewById(R.id.imagebtn1);
		time20 = (ImageButton) findViewById(R.id.imagebtn2);
		time25 = (ImageButton) findViewById(R.id.imagebtn3);
		time30 = (ImageButton) findViewById(R.id.imagebtn4);
		time35 = (ImageButton) findViewById(R.id.imagebtn5);
		time40 = (ImageButton) findViewById(R.id.imagebtn6);
		time45 = (ImageButton) findViewById(R.id.imagebtn7);

		time15.setOnClickListener(this);
		time20.setOnClickListener(this);
		time25.setOnClickListener(this);
		time30.setOnClickListener(this);
		time35.setOnClickListener(this);
		time40.setOnClickListener(this);
		time45.setOnClickListener(this);
	}

	@Override
	public void onClick(View v) {

		if (v.getId() == R.id.imagebtn1) {
			timeSelected(15);
			selectedTime = 15;

		} else if (v.getId() == R.id.imagebtn2) {
			timeSelected(20);
			selectedTime = 20;

		} else if (v.getId() == R.id.imagebtn3) {
			timeSelected(25);
			selectedTime = 25;

		} else if (v.getId() == R.id.imagebtn4) {
			timeSelected(30);
			selectedTime = 30;

		} else if (v.getId() == R.id.imagebtn5) {
			timeSelected(35);
			selectedTime = 35;

		} else if (v.getId() == R.id.imagebtn6) {
			timeSelected(40);
			selectedTime = 40;

		} else if (v.getId() == R.id.imagebtn7) {
			timeSelected(45);
			selectedTime = 45;

		}

	}

	public void selecttime(int time) {
		time = time - 10;
		final Calendar c = Calendar.getInstance();
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		c.add(Calendar.MINUTE, time);
		final String strDate = sdf.format(c.getTime());

		isInternetPresent = cd.isConnection();
		if (isInternetPresent) {

			ParseQuery<ParseObject> putTime = ParseQuery
					.getQuery(Prefs.table_registedcar);
			putTime.whereEqualTo(Prefs.table_col_carid, car);
			putTime.getFirstInBackground(new GetCallback<ParseObject>() {
				@Override
				public void done(ParseObject object, ParseException e) {
					if (object != null) {
						object.put(Prefs.table_col_timeselected, strDate);
						object.saveInBackground(new SaveCallback() {

							@Override
							public void done(ParseException e) {
								Log.v("alarm time", c.getTime().toString());
								Intent intent = new Intent(
										getApplicationContext(),
										AlarmReceiver.class);
								intent.putExtra("car_id", car);
								PendingIntent pendingIntent = PendingIntent
										.getBroadcast(getApplicationContext(),
												1, intent, 0);
								AlarmManager alarmManager = (AlarmManager) getApplicationContext()
										.getSystemService(
												getApplicationContext().ALARM_SERVICE);
								alarmManager.cancel(pendingIntent);
								alarmManager.set(AlarmManager.RTC_WAKEUP,
										c.getTimeInMillis(), pendingIntent);
								// sendNotification();
							}
						});
					} else {

						TimeSelectScreen.this.finish();
						ParkingCommon.getinstance(TimeSelectScreen.this)
								.okAlertDialog(
										getResources()
												.getString(R.string.su_ca));
					}
				}
			});
		} else {
			ParkingCommon.getinstance(TimeSelectScreen.this).okAlertDialog(
					getResources().getString(R.string.no_internet));
		}

	}

	public void timeSelected(int time) {

		switch (time) {
		case 15:
			time15.setBackgroundResource(R.drawable.select_custom_border);
			time20.setBackgroundResource(R.drawable.custom_border);
			time25.setBackgroundResource(R.drawable.custom_border);
			time30.setBackgroundResource(R.drawable.custom_border);
			time35.setBackgroundResource(R.drawable.custom_border);
			time40.setBackgroundResource(R.drawable.custom_border);
			time45.setBackgroundResource(R.drawable.custom_border);
			timeok.setBackgroundResource(R.drawable.custom_border);
			timecancel.setBackgroundResource(R.drawable.custom_border);
			break;

		case 20:
			time20.setBackgroundResource(R.drawable.select_custom_border);
			time15.setBackgroundResource(R.drawable.custom_border);
			time25.setBackgroundResource(R.drawable.custom_border);
			time30.setBackgroundResource(R.drawable.custom_border);
			time35.setBackgroundResource(R.drawable.custom_border);
			time40.setBackgroundResource(R.drawable.custom_border);
			time45.setBackgroundResource(R.drawable.custom_border);
			timeok.setBackgroundResource(R.drawable.custom_border);
			timecancel.setBackgroundResource(R.drawable.custom_border);
			break;

		case 25:
			time25.setBackgroundResource(R.drawable.select_custom_border);
			time20.setBackgroundResource(R.drawable.custom_border);
			time15.setBackgroundResource(R.drawable.custom_border);
			time30.setBackgroundResource(R.drawable.custom_border);
			time35.setBackgroundResource(R.drawable.custom_border);
			time40.setBackgroundResource(R.drawable.custom_border);
			time45.setBackgroundResource(R.drawable.custom_border);
			timeok.setBackgroundResource(R.drawable.custom_border);
			timecancel.setBackgroundResource(R.drawable.custom_border);
			break;

		case 30:
			time30.setBackgroundResource(R.drawable.select_custom_border);
			time20.setBackgroundResource(R.drawable.custom_border);
			time25.setBackgroundResource(R.drawable.custom_border);
			time15.setBackgroundResource(R.drawable.custom_border);
			time35.setBackgroundResource(R.drawable.custom_border);
			time40.setBackgroundResource(R.drawable.custom_border);
			time45.setBackgroundResource(R.drawable.custom_border);
			timeok.setBackgroundResource(R.drawable.custom_border);
			timecancel.setBackgroundResource(R.drawable.custom_border);
			break;

		case 35:
			time35.setBackgroundResource(R.drawable.select_custom_border);
			time20.setBackgroundResource(R.drawable.custom_border);
			time25.setBackgroundResource(R.drawable.custom_border);
			time30.setBackgroundResource(R.drawable.custom_border);
			time15.setBackgroundResource(R.drawable.custom_border);
			time40.setBackgroundResource(R.drawable.custom_border);
			time45.setBackgroundResource(R.drawable.custom_border);
			timeok.setBackgroundResource(R.drawable.custom_border);
			timecancel.setBackgroundResource(R.drawable.custom_border);
			break;

		case 40:
			time40.setBackgroundResource(R.drawable.select_custom_border);
			time20.setBackgroundResource(R.drawable.custom_border);
			time25.setBackgroundResource(R.drawable.custom_border);
			time30.setBackgroundResource(R.drawable.custom_border);
			time35.setBackgroundResource(R.drawable.custom_border);
			time15.setBackgroundResource(R.drawable.custom_border);
			time45.setBackgroundResource(R.drawable.custom_border);
			timeok.setBackgroundResource(R.drawable.custom_border);
			timecancel.setBackgroundResource(R.drawable.custom_border);
			break;

		case 45:
			time45.setBackgroundResource(R.drawable.select_custom_border);
			time20.setBackgroundResource(R.drawable.custom_border);
			time25.setBackgroundResource(R.drawable.custom_border);
			time30.setBackgroundResource(R.drawable.custom_border);
			time35.setBackgroundResource(R.drawable.custom_border);
			time40.setBackgroundResource(R.drawable.custom_border);
			time15.setBackgroundResource(R.drawable.custom_border);
			timeok.setBackgroundResource(R.drawable.custom_border);
			timecancel.setBackgroundResource(R.drawable.custom_border);
			break;
		}
	}

	public void isTime() {

	}

	public boolean isForeground(String myPackage) {
		ActivityManager manager = (ActivityManager) getSystemService(Activity.ACTIVITY_SERVICE);
		List<ActivityManager.RunningTaskInfo> runningTaskInfo = manager
				.getRunningTasks(1);

		ComponentName componentInfo = runningTaskInfo.get(0).topActivity;
		if (componentInfo.getPackageName().equals(myPackage))
			return true;

		return false;
	}
}
