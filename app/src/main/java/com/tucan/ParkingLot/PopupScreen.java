package com.tucan.ParkingLot;



import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.Toast;

import com.parse.GetCallback;
import com.parse.ParseException;
import com.parse.ParseObject;
import com.parse.ParseQuery;

public class PopupScreen extends Activity {
	ImageButton closebtn;
	Boolean isInternetPresent = false;
	CheckInternetConnection cd;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.popupscreen);
		
		cd = new CheckInternetConnection(getApplicationContext());

		closebtn = (ImageButton) findViewById(R.id.button1);
		isInternetPresent = cd.isConnection();
		if (isInternetPresent) {
		closebtn.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View arg0) {
				final ProgressDialog progressDialog = new ProgressDialog(
						PopupScreen.this);
				progressDialog.setMessage("Por favor,espera.....");
				progressDialog.setIndeterminate(true);
				progressDialog.setCancelable(false);
				progressDialog.show();

				SharedPreferences sharedPreferences = getSharedPreferences(
						Prefs.local_cust_table, Context.MODE_PRIVATE);
				final String carid = sharedPreferences.getString(
						Prefs.local_col_carid, "");

				ParseQuery<ParseObject> query = ParseQuery
						.getQuery(Prefs.table_registedcar);
				query.whereEqualTo(Prefs.table_col_carid, carid);
				query.getFirstInBackground(new GetCallback<ParseObject>() {

					@Override
					public void done(ParseObject object, ParseException e) {
						if (object != null) {
							Intent in = new Intent(PopupScreen.this,
									TimeSelectScreen.class);
							startActivity(in);
							PopupScreen.this.finish();
							progressDialog.dismiss();
						} else {
							progressDialog.dismiss();
							Intent in = new Intent(PopupScreen.this,
									PopUpForCorrectId.class);
							startActivity(in);
							PopupScreen.this.finish();

						}

					}
				});

			}
		});
		}else {

			Toast.makeText(PopupScreen.this,
					getResources().getString(R.string.no_internet),// msg for no
														// internet
														// connection
					Toast.LENGTH_LONG).show();
		}
	}
}
