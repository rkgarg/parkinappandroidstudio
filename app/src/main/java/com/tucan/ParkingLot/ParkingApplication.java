package com.tucan.ParkingLot;

import java.util.List;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.app.Application;
import android.util.Log;

import com.parse.FindCallback;
import com.parse.Parse;
import com.parse.ParseACL;
import com.parse.ParseCrashReporting;
import com.parse.ParseException;
import com.parse.ParseInstallation;
import com.parse.ParseObject;
import com.parse.ParsePush;
import com.parse.ParseQuery;
import com.parse.SaveCallback;
import com.splunk.mint.Mint;

public class ParkingApplication extends Application {

	@Override
	public void onCreate() {

		super.onCreate();
		final String deviceid = "user"
				+ ParkingCommon.getinstance(ParkingApplication.this)
						.getDeviceID();
		Mint.initAndStartSession(this, "0d1c69a4");

		// Initialize Crash Reporting.
		ParseCrashReporting.enable(this);

		// Enable Local Datastore.
		Parse.enableLocalDatastore(this);

		// Add your initialization code here
		Parse.initialize(this, "KV0np8uGhPP6MECeF1G90v7zVCy6J0ILfD1HHqqB",
				"WX7tZ7ZF0xAPZhb5ouTydta1uP4uOwqRRZ3SSRsU");
		ParseACL defaultACL = new ParseACL();
		// Optionally enable public read access.
		defaultACL.setPublicReadAccess(true);
		defaultACL.setPublicWriteAccess(true);
		ParseACL.setDefaultACL(defaultACL, true);
		 ParsePush.subscribeInBackground(deviceid, new SaveCallback() {

			@Override
			public void done(com.parse.ParseException e) {
				if (e == null) {
					Log.d("com.parse.push",
							"successfully subscribed to the broadcast channel.");
				} else {
					Log.e("com.parse.push", "failed to subscribe for push", e);
				}

			}
		 });
		

	
		
	}
	
}
