package com.tucan.ParkingLot;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.TextView;

public class Registration extends Activity implements OnClickListener {
Boolean IsInternetPresent=false;
CheckInternetConnection cd;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		 getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
		 WindowManager.LayoutParams.FLAG_FULLSCREEN);

		super.onCreate(savedInstanceState);
		setContentView(R.layout.registration);
		cd = new CheckInternetConnection(getApplicationContext());
		MyTextView regBtn1 = (MyTextView) findViewById(R.id.CarRegistration);
		regBtn1.setOnClickListener(this);

		MyTextView regBtn2 = (MyTextView) findViewById(R.id.RegistedCar);
		regBtn2.setOnClickListener(this);
		

	}

	@Override
	public void onClick(View v) {
		if (v.getId() == R.id.CarRegistration) {
			Intent i = new Intent(Registration.this, CarRegistration.class);
			startActivity(i);
			
		} else if (v.getId() == R.id.RegistedCar) {
			IsInternetPresent = cd.isConnection();
			if(IsInternetPresent==true) {
			Intent in = new Intent(Registration.this, RegisteredCars.class);
			startActivity(in);
			}else{
				ParkingCommon.getinstance(Registration.this).IsInternetPresent();
			}
			
		}
	}

}
